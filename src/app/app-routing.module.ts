import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ImageComponent } from './modules/image/image.component';
import { BreedListComponent } from './modules/breed-list/breed-list.component';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'image',
    pathMatch: 'full'
  },
  {
    path: 'breeds',
    component: BreedListComponent,
    pathMatch: 'full'
  },
  {
    path: 'image',
    component: ImageComponent,
    pathMatch: 'full'
  },
  {
    path: 'mascots',
    component: BreedListComponent,
    pathMatch: 'full'
  },
];
@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
