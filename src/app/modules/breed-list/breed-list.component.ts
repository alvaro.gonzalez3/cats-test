import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { Subscription } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { MainDataService } from 'src/app/services/mainData.service';

interface Breed {
  photo?: string,
  name?: string,
  description?: string,
  id?: string,
  mascot?: string,
}

@Component({
  selector: 'app-breed-list',
  templateUrl: './breed-list.component.html',
  styleUrls: ['./breed-list.component.scss']
})


export class BreedListComponent implements OnInit {

  private subBreeds: Subscription;
  private subId: Subscription;

  public page: number = 0;

  public breeds: Breed[] = [];
  public breedsToShow: Breed[] = [];
  public breedsByPage: Breed[] = [];

  public index: number = 0;
  public limit: number = 5;
  private filterText: string; 

  public namesForm: FormGroup;
  public mascotPage: boolean = false;
  constructor(
    private mainDataService: MainDataService,
    private fb: FormBuilder,
  ) {}

  ngOnInit(): void {
    //Me suscribo a los observables del servicio que corresponden a cada llamada
    this.subBreeds = this.mainDataService.mainObservable()
    .pipe(filter((data) => !!data?.length))
    .subscribe((data) => {
      this.onBreeds(data);
    })

    this.subId = this.mainDataService.idObservable()
    .pipe(map((data) => data[0]?.url))
    .subscribe((data: string) => {
      this.onBreedId(data);
    });
    //Me traigo los datos iniciales para pintar si estamos en la pantalla de mascotas sino, llamo a la api y recibo los datos
    if(window.location.href.includes('mascots')){
      this.getMascotData();
      //Indico que estamos en la pantalla de mascotas
      this.mascotPage = true;
    }
    else{
      this.getData();
    }
  }

  private onBreeds(breeds){
    this.breeds = [];
    //Creo el array de objetos que voy a pintar solo con los datos necesarios
    breeds.forEach(breed =>{
      let currBreed: Breed = {
        name: breed?.name,
        description: breed?.description,
        id: breed?.id,
        photo: '',
        mascot:'',
      };
      this.breeds.push(currBreed);
    });
    //Me traigo la foto del primero y hago un bucle para traerlas todas y añadirlas a su objeto correspondiente
    this.getDataById(this.breeds[0]?.id);
  }

  private onBreedId(data:string){
    this.breeds[this.index].photo = data;
    this.index++;
    
    if(this.index <= (this.limit - 1)){
      this.getDataById(this.breeds[this.index]?.id);
    }
    else{
      this.breedsToShow = this.breeds;
      this.createForm()
      if(this.filterText){
        this.onFilter(this.filterText)
      }
      return
    }
  }

  public changePage(type: number){
    this.index = 0;
    if(type>1){
      this.page += 1;
    }
    else{
      this.page -= 1;
    }
    if(this.mascotPage){
      this.controlMascotPage();
    }
    else{
      this.getData();
    }
  }

  private controlMascotPage(){
    //Como en local storage no puedo paginar, me traigo todo y a raíz de ello pagino a mano
    this.breedsToShow = [];
    this.breeds.forEach((breed, index) => {
      if(index >= (this.page * this.limit) && index < ((this.page * this.limit) + 5)){
        this.breedsToShow.push(breed);
      }
    })
    this.breedsByPage = this.breedsToShow;
    if(this.filterText){
      this.onFilter(this.filterText);
    }
  }

  private getData(){
    this.mainDataService.getData('https://api.thecatapi.com/v1/breeds', this.limit, this.page);
  }

  private getDataById(id: string){
    this.mainDataService.getDataById('https://api.thecatapi.com/v1/images/search', id);
  }

  private getMascotData(){
    //Recibo todo lo guardado en el localstorage
    let keys = Object.keys(localStorage);
    keys.forEach(key => {
      this.breeds.push(JSON.parse(localStorage.getItem(key)));
    });
    this.breeds.forEach((breed, index) => {
      if(index < this.limit){
        this.breedsToShow.push(breed);
      }
    })
    this.breedsByPage = this.breedsToShow;
    this.createForm();    
  }

  public onFilter(text: string){
    //Filtro por el texto del input, función que se llama cuando se cambia de página también filtrando por el texto existente
    //Lo óptimo sería poder pasar este texto como parámetro a la petición para que filtre todos los gatos, no solo los de la página actual, pero como no admitía tal parámetro, decidí hacerlo así
    this.filterText = text;
    if(this.mascotPage){
      if(!this.filterText){
        this.controlMascotPage();
      }
      else{
        this.breedsToShow = this.filterItems(this.breedsByPage);
      }
    }else{
      if(!this.filterText){
        this.breedsToShow = this.breeds;
      }
      else{
        this.breedsToShow = this.filterItems(this.breeds);
      }
    }
  }

  private filterItems(breeds: Breed[]){    
    //Uso el lowecase para que no importe cómo lo busca en el filtro
    return breeds.filter(breed => {     
      let nameLC = breed?.name.toLocaleLowerCase();
      let textLC = this.filterText?.toLocaleLowerCase();   
      return nameLC.includes(textLC);
    })
  }

  private createForm(){
    //Creo un formulario que se resetea cada vez que se cambia de página para poder tener todo controlado
    this.namesForm = this.fb.group({});
    this.breeds.forEach((breed, index) => {
      if(JSON.parse(localStorage.getItem(breed?.id))){
        console.log(JSON.parse(localStorage.getItem(breed?.photo)));
        
        this.breedsToShow[index].photo = (JSON.parse(localStorage.getItem(breed?.id)))?.photo
      }
      this.namesForm.addControl(breed?.id, new FormControl(JSON.parse(localStorage.getItem(breed?.id))?.mascot || ''));
    })    
  }

  public addMascot(breed: Breed){
    //Añado el nombre al localstorage ya que no hay base de datos donde pueda guardar el dato
    breed.mascot = this.namesForm?.get(breed.id)?.value;
    localStorage.setItem(breed?.id, JSON.stringify(breed));
    window.alert(`${breed?.mascot} has been added to your pets!`)
  }
}
