import { Component, OnDestroy, OnInit } from '@angular/core';
import { Observable, Subscription } from 'rxjs';
import { map } from 'rxjs/operators';
import { MainDataService } from 'src/app/services/mainData.service';

@Component({
  selector: 'app-image',
  templateUrl: './image.component.html',
  styleUrls: ['./image.component.scss']
})
export class ImageComponent implements OnInit, OnDestroy {

  public imgURL: string;
  private subImage: Subscription;

  constructor(
    private mainDataService: MainDataService
  ) {}

  ngOnInit(): void {

    //Inicializo la subscripción al servicio
    this. subImage = this.mainDataService.mainObservable()
    .pipe(map((data) => data[0]?.url))
    .subscribe((data: string) => {
      this.onImage(data)
    })

    //Hago la primera llamada pra recibir la foto
    this.getRandomImage()
  }

  private onImage(image: string){   
    
    //Le doy valor a la variable que voy a usar en el html
    this.imgURL = image
  }

  public getRandomImage(){

    //Llamo al servicio para recibir la data
    this.mainDataService.getData('https://api.thecatapi.com/v1/images/search')
  }

  ngOnDestroy(): void {

    //Termino la suscripción
    this.subImage.unsubscribe()
  }
}
