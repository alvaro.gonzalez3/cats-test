import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable, Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})

export class MainDataService {

  constructor(
    private http: HttpClient
  ) {}

  public subscription$ = new Subject();
  public subscriptionId$ = new Subject();

  public getData(url: string, limit?: number, page?: number){
    //Llamo a la api y le paso los datos al Subject para recogerlos mediante la suscripción
    let params = new HttpParams();

    if(limit){
      params = params.append('limit', limit.toString() )      

    if(page){
      params = params.append('page', page.toString())
    }
    }
    this.http.get<any>(url, {params: params}).subscribe(data =>{
      this.subscription$.next(data)
  })
  }
  public getDataById(url: string, id: string){
    let params = new HttpParams();
    params = params.append('breed_ids', id)     
    this.http.get<any>(url, {params: params}).subscribe(data =>{
      this.subscriptionId$.next(data)
  })
  }
  public mainObservable(): Observable<any>{
    //Retorno el subject como observable para poder subscribirme a él.
    return this.subscription$.asObservable()
  }

  public idObservable(): Observable<any>{
    return this.subscriptionId$.asObservable()
  }
}